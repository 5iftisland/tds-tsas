// A mock function to mimic making an async request for data
// const URLState = {
//   value: 0,
  
// }
// export function fetchCount(amount = 1) {
//   return new Promise<{ data:typeof URLState }>((resolve) =>
//     setTimeout(() => resolve({ data:{value:amount} }), 500)
//   );
// }
export function fetchCount(amount = 1) {
  return new Promise<{ data:number }>((resolve) =>
    setTimeout(() => resolve({ data:amount }), 500)
  );
}
