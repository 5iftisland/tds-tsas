
import axios from 'axios';
import {
  selectToken,
} from '../Slice/TokenSlice';

interface header {
  'Content-Type': string,
  token?: string
}

interface parameter {
    Data: any
    headers?: header;
    params?: string,
    Token?: string
}

export function RESTfulPost<T extends parameter> (Url: string, payload:T) { 
  // const Token = useAppSelector(selectToken);
  // console.log(Token)
  let headers:any = {}      
  if(payload.headers) {
    headers = payload.headers
  } else {
    headers = {  
      'Content-Type': 'application/json',
      // token: Token
      // Authorization: `Bearer ${payload.Token}`// Token
    }
  }    
  if(payload.Token){
    headers['Authorization']=`Bearer ${payload.Token}`// Token
  } 
  const response = axios
      .post(Url, payload.Data, {
        headers: headers,
        params: payload.params
      })
      .then(response => {
        return response.data
      })
      .catch(function (error) { // 请求失败处理
        console.log(error)
      })
  return response
}

export function RESTfulGet<T extends parameter> (Url: string, payload:T) { 
  
  // console.log(Token)
  let headers:any = {}      
  if(payload.headers) {
    headers = payload.headers
  } else {
    headers = {  
      'Content-Type': 'application/json',
      // token: Token
      // Authorization: payload.URL === loginURL ? null : 'Bearer ' + context.state.token
    }
  }     
  if(payload.Token){
    headers['Authorization']=`Bearer ${payload.Token}`// Token
  } 
  const response = axios
      .get(Url, {
        headers: headers,
        params: payload.params
      })
      .then(response => {
        return response.data
      })
      .catch(function (error) { // 请求失败处理
        console.log(error)
      })
  return response
}
