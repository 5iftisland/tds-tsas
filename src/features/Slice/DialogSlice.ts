import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface CounterState {
  value: boolean;
}

const initialState: CounterState = {
  value: false,
};

export const DialogSlice = createSlice({
  name: 'Dialog',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    SetDialog: (state, action: PayloadAction<boolean>) => {
      state.value = action.payload;
    },
  },
});

export const { SetDialog } = DialogSlice.actions;

export const selectDialog = (state: RootState) => state.Dialog.value;

export default DialogSlice.reducer;
