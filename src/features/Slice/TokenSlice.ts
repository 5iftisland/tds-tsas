import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface CounterState {
  value: string;
}

const initialState: CounterState = {
  value: '',
};

export const tokenSlice = createSlice({
  name: 'token',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    SetToken: (state, action: PayloadAction<string>) => {
      state.value = action.payload;
    },
  },
});

export const { SetToken } = tokenSlice.actions;

export const selectToken = (state: RootState) => state.token.value;

export default tokenSlice.reducer;
