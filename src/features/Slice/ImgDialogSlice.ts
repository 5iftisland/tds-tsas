import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface CounterState {
  value: boolean;
}

const initialState: CounterState = {
  value: false,
};

export const ImgDialogSlice = createSlice({
  name: 'imgDialog',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    SetImgDialog: (state, action: PayloadAction<boolean>) => {
      state.value = action.payload;
    },
  },
});

export const { SetImgDialog } = ImgDialogSlice.actions;

export const selectImgDialog = (state: RootState) => state.imgDialog.value;

export default ImgDialogSlice.reducer;
