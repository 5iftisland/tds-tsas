import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from '../features/Slice/counterSlice';
import tokenReducer from '../features/Slice/TokenSlice';
import imgDialogReducer from '../features/Slice/ImgDialogSlice';
import DialogSlice from '../features/Slice/DialogSlice';
export const store = configureStore({
  reducer: {
    counter: counterReducer,
    token: tokenReducer,
    imgDialog: imgDialogReducer,
    Dialog: DialogSlice
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
