import React from 'react'
import { Counter } from './Components/Counter'
import AppRoute from './Components/Route/AppRoute'
import './assets/scss/App.scss'
import logo from './assets/img/logo512.png'//'./assets/img/logo.svg';

import {theme} from './Components/color/Color'
import { ThemeProvider } from '@mui/material/styles';

function App() {

  return (
    <div className="App">
      
      <ThemeProvider theme={theme}>
        <AppRoute/>
      </ThemeProvider>
      
      {/* <header className="App-header">
        <AppRoute/>
        <img
          // src={`${process.env.PUBLIC_URL ?? ''}/logo.svg`}
          src={logo}
          className="App-logo"
          alt="logo"
        />
        <Counter />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <span>
          <span>Learn </span>
          <a
            className="App-link"
            href="https://reactjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux-toolkit.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux Toolkit
          </a>
          ,<span> and </span>
          <a
            className="App-link"
            href="https://react-redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React Redux
          </a>
        </span>
      </header>
       */}

    </div>
  )
}

export default App
