import React, { useState } from 'react';
import Advertise from './Index/Advertise'
import ImgBtn from './Index/ImgBtn'
import CommodityCard from './Index/CommodityCard'
import {
    Grid,
} from '@mui/material';

const Index = () => {
    const items=[0,1,2,3]
    let lists =items.map((item, index) => {
        return( 
        <Grid item xs={3} key={index}>
            <CommodityCard/>
          </Grid>)
        })
    return (
      <div className='App-content'>
        <Advertise/>
        <ImgBtn />
        <Grid container spacing={2}>
          {
          items.map((item, index) => {
            return ( 
              <Grid item xs={6} md={3} key={index}>
                <CommodityCard/>
              </Grid>
            )
          })
        }
        </Grid>
        
        
       
      </div>
    );
}


export default Index;
