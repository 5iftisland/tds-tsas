import * as React from 'react';
import { useAppDispatch } from '../../app/hooks';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import {
  SetDialog,
} from '../../features/Slice/DialogSlice';
export default function MaterialsCard(props:Prop) {
  const dispatch = useAppDispatch();
  const EditClick=()=>{
    props.UpDate('CropCard','setModel','edit')
    dispatch(SetDialog(true))
  }
  const InfoClick=()=>{
    props.UpDate('CropCard','setModel','info')
    dispatch(SetDialog(true))
  }
  const DeleteClick=()=>{
    props.UpDate('CropCard','setModel','delete')
    dispatch(SetDialog(true))
  }
  return (
    <Card className='crop-card'>
      <CardContent>
        <Typography
          component="span"     
        >
          行政區: 後壁區
        </Typography>
        <Typography
          component="span"     
        >
          面積: 0.2公頃
        </Typography>
        <Typography
          component="span"     
        >
          上次農噴時間:
        </Typography>  
      </CardContent>
      <CardActions sx={{justifyContent:'flex-end'}}>
        <Button sx={{fontSize:'1.5rem'}} onClick={() => InfoClick()}>詳細資訊</Button>
        <Button sx={{fontSize:'1.5rem'}} onClick={() => EditClick()}>修改</Button>
        <Button sx={{fontSize:'1.5rem',color:'red'}} onClick={() => DeleteClick()}>刪除</Button>
      </CardActions>
    </Card>
  );
}
interface Prop {
  UpDate: Function 
}
