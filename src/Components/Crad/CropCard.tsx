import * as React from 'react';
import { useAppDispatch } from '../../app/hooks';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import {
  SetDialog,
} from '../../features/Slice/DialogSlice';
const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    •
  </Box>
);

export default function CropCard(props:Prop) {
  const dispatch = useAppDispatch();
  const EditClick=()=>{
    props.UpDate('CropCard','setModel','edit')
    dispatch(SetDialog(true))
  }
  return (
    <Card className='crop-card'>
      <CardContent>
        <Typography
          component="span"     
        >
          物種名稱:
        </Typography>
        <Typography
          component="span"     
        >
          品種名稱:
        </Typography>
        <Typography
          component="span"     
        >
          認證類型:
        </Typography>   
      </CardContent>
      <CardActions sx={{justifyContent:'flex-end'}}>
        <Button sx={{fontSize:'1.5rem'}} onClick={() => EditClick()}>修改</Button>
        <Button sx={{fontSize:'1.5rem',color:'red'}}>刪除</Button>
      </CardActions>
    </Card>
  );
}
interface Prop {
  UpDate: Function 
}
