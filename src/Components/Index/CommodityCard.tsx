import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ButtonBase from '@mui/material/ButtonBase';
import { styled } from '@mui/material/styles';
export default function CommodityCard() {
 
  return (
    <ButtonBase>
      <Card  sx={{ maxWidth: 345,mt:2,mr:2, fontSize:'1.8rem',textAlign:'left' }} onClick={(e)=>{console.log('132')}}> 
        <CardMedia
          component="img"
          sx={{height:{ xs: '120', md: "320" }}}
          
          image="https://source.unsplash.com/featured/?macbook"
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom  component="div" sx={{fontSize:'3rem'}}>
            Lizard
          </Typography>
          <Typography component="div" sx={{fontSize:'1.8rem'}} >
            廠商名稱
          </Typography>
          <Typography component="div" sx={{fontSize:'1.8rem'}} >
            NT$ 700
          </Typography>
        </CardContent>
        {/* <CardActions>
          <Button size="small">Share</Button>
          <Button size="small">Learn More</Button>
        </CardActions> */}
      </Card>
    </ButtonBase>
      
  );
}
