import React, { useState } from 'react';
import Settings, { DefaultSettingsT, SettingsT } from './Settings';
import Carousel from 'react-material-ui-carousel';

import {
    Card,
    CardContent,
    CardMedia,
    Typography,
    Grid,
    Button,
} from '@mui/material';


const Advertise = () => {

    const [settings, setSettings] = useState<SettingsT>(DefaultSettingsT);

    return (
        <div className='Advertise'>
            {/* <br/> */}
            <Carousel
              {...settings}
            >
              {
                items.map((item, index) => {
                  return <Banner item={item} key={index}  />
                })
              }
            </Carousel>
            {/* <br/> */}
            {/* Carousel 設定 */}
            {/* <Settings settings={settings} setSettings={setSettings}/> */}
        </div>
    );
}


type Item = {
    Name: string,
    Image: string
    // Caption: string,
    // contentPosition: "left" | "right" | "middle",
    // Items: {Name: string, Image: string}[]
}

interface BannerProps
{
    item: Item,
    length?: number,
}

const Banner = (props: BannerProps) => {

    const item = props.item;
    const media = (
      <Grid item xs={12} key={item.Name}>
        <CardMedia
          className="Media"
          image={item.Image}
          title={item.Name}
        >
          <Typography className="MediaCaption">
            {item.Name}
          </Typography>
        </CardMedia>
      </Grid>
    )

    return (
        <Card raised className="Banner">
            <Grid container spacing={0} className="BannerGrid">
                {media}
            </Grid>
        </Card>
    )
}

const items: Item[] = [
  {
    Name: "Macbook Pro",
    Image: "https://source.unsplash.com/featured/?macbook"
  },
  {
    Name: "iPhone",
    Image: "https://source.unsplash.com/featured/?iphone"
  },
  {
    Name: "Learus Vacuum Cleaner",
    Image: "https://source.unsplash.com/featured/?vacuum,cleaner"
  }
]


export default Advertise;
