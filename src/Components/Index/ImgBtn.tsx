import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import ButtonBase from '@mui/material/ButtonBase';
import Typography from '@mui/material/Typography';

const images = [
  {
    url: 'https://source.unsplash.com/featured/?macbook',
    title: 'Breakfast',
    width: '150px',
    height: '150px'
  },
  {
    url: 'https://source.unsplash.com/featured/?macbook',
    title: 'Breakfast1',
    width: '10vw',
    height: '10vw'
  },
  {
    url: '',
    title: 'Breakfast2',
    width: '10vw',
    height: '10vw'
  },
  {
    url: '',
    title: 'Camera1',
    width: '10vw',
    height: '10vw'
  },
  {
    url: '',
    title: 'Camera2',
    width: '10vw',
    height: '10vw'
  }
];

const ImageButton = styled(ButtonBase)(({ theme }) => ({
  position: 'relative',
  height: 150,
  marginRight: '50px',
  [theme.breakpoints.down('sm')]: {
    //width: '10% !important', // Overrides inline-style
    height: 100,
  },
  '&:hover, &.Mui-focusVisible': {
    zIndex: 1,
    '& .MuiImageBackdrop-root': {
      opacity: 0.15,
    },
    '& .MuiImageMarked-root': {
      opacity: 0,
    },
    '& .MuiTypography-root': {
      border: '0px solid currentColor',
      pending: '1rem'
    }
  },
}));

const ImageSrc = styled('span')({
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  backgroundSize: 'cover',
  backgroundPosition: 'center 40%',
});

const ImageText = styled('span')(({ theme }) => ({
  
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  color: theme.palette.common.white,
}));

const ImageBackdrop = styled('span')(({ theme }) => ({
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  backgroundColor: theme.palette.common.black,
  opacity: 0.4,
  transition: theme.transitions.create('opacity'),
}));

const ImageMarked = styled('span')(({ theme }) => ({
  height: 3,
  width: 18,
  backgroundColor: theme.palette.common.white,
  position: 'absolute',
  bottom: -2,
  left: 'calc(50% - 9px)',
  transition: theme.transitions.create('opacity'),
}));

export default function ButtonBases() {
  return (
    <Box sx={{ display: 'flex', flexWrap: 'wrap',  width: '100%', justifyContent: 'center' }}>
      {images.map((image) => (
        <ImageButton
          focusRipple
          key={image.title}
          className='img-button'
        >
          {/* 顯示圖片 */}
          <ImageSrc style={{ backgroundImage: `url(${image.url})` }} />
          {/* 沒圖片時，顯示的背景 */}
          <ImageBackdrop className="MuiImageBackdrop-root" /> 
          <Typography
              component="div"
              className='text'
            >
              {image.title}
             
            </Typography>
          {/* <ImageText >
            <Typography
              component="span"
              variant="subtitle1"
              color="inherit"
              sx={{
                position: 'relative',
                p: 4,
                pt: 2,
                pb: (theme) => `calc(${theme.spacing(1)} + 6px)`,
                fontSize: '2rem'
              }}
            >
              {image.title}
              <ImageMarked className="MuiImageMarked-root" />
            </Typography>
          </ImageText> */}
        </ImageButton>
      ))}
    </Box>
  );
}
