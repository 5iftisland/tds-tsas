import * as React from 'react';
import { useAppDispatch } from '../../app/hooks';
import {
  Box,
  SwipeableDrawer,
  List,
  Divider,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  IconButton,
  Avatar,
  Typography
} from '@mui/material';
import {useNavigate} from 'react-router-dom'
import MenuIcon from '@mui/icons-material/Menu';
import PersonIcon from '@mui/icons-material/Person';
import {
  SetToken
} from '../../features/Slice/TokenSlice';
type Anchor = 'top' | 'left' | 'bottom' | 'right';

export default function SwipeableTemporaryDrawer() {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  function ChangRoute(path:string) {
    if(path==='/logout'){
      sessionStorage.removeItem("token");
      dispatch(SetToken(''))
      navigate('/')
    } else {
      navigate(path)
    }
    
  }

  const [listItemName, setListItemName] = React.useState([
    {name:'田間管理',path: '/Farm/Crop'},
    {name:'機務管理',path: ''},
    {name:'付款紀錄',path: ''},
    {name:'收款紀錄',path: ''},
    {name:'商品上架',path: ''},
    {name:'功能導覽',path: ''},
    {name:'系統客服',path: ''},
    {name:'版本說明',path: ''},
    {name:'登出',path: '/logout'},
  ]);
  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
    };

  const list = (anchor: Anchor) => (
    <Box
      className='Drawer'
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      {/* <Box className='List-head'>
        <Avatar sx={{ m: 0 }}>
          <PersonIcon ></PersonIcon>
        </Avatar>
        <div className='title'>
          智農網
          <div className='sub-title'>
            編輯帳戶資訊
          </div>
        </div>
        <div className='status'>
          審核中
        </div>
          
        
      </Box> */}
      <Divider />
      <List className='List-content'>
        <ListItem className='List-FirstItem' disablePadding>
          <ListItemButton onClick={()=>ChangRoute('/Account/Information')}>
            <Avatar sx={{ m: 0 }}>
              <PersonIcon ></PersonIcon>
            </Avatar>
            <div className='title'>
              智農網
              <div className='sub-title'>
                編輯帳戶資訊
              </div>
            </div>
            <div className='status'>
              審核中
            </div>
          </ListItemButton>
        </ListItem>
        <Divider />
        {listItemName.map((obj, index) => (
          <ListItem key={obj.name} disablePadding>
            <ListItemButton onClick={()=>ChangRoute(obj.path)}>
              <ListItemIcon>
                <MenuIcon />
              </ListItemIcon>
              <ListItemText  primary={obj.name} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <div>
       
          <IconButton onClick={toggleDrawer('left', true)} sx={{color:'#AAAAAA'}}>
            <MenuIcon  sx={{ mr: 1 ,fontSize:'3.6rem'}} />
          </IconButton>
          <SwipeableDrawer
            anchor={'left'}
            open={state['left']}
            onClose={toggleDrawer('left', false)}
            onOpen={toggleDrawer('left', true)}
          >
            {list('left')}
          </SwipeableDrawer>
      
      
    </div>
  );
}
