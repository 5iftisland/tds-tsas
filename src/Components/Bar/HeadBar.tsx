import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import logo from '../../assets/img/logo512.png';//'./assets/img/logo.svg';
import Badge from '@mui/material/Badge';
import PersonIcon from '@mui/icons-material/Person';
import {useNavigate} from 'react-router-dom';
import Drawer from './Drawer'
import {
    Outlet
} from 'react-router-dom'
const pages = ['服務', '接單', '訂單', '商品'];
const settings = ['服務', '接單', '訂單', '商品'];

const ResponsiveAppBar = () => {
  const navigate = useNavigate();
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    // setAnchorElUser(event.currentTarget);
    navigate('/login')
    // navigate('/Account/Information')
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  
  function ChangRoute(path:string) {
    navigate(path)
  }
  return (
    <Box
        component="div"
      >
      <AppBar position="static" color='default'  className='App-Bar'>
        <Container maxWidth="xl" >
          <Toolbar disableGutters className='App-Text'>
            <Drawer/>
            {/* <Box sx={{ flexGrow: 0, display: { xs: 'flex', md: 'none' }}}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: { xs: 'block', md: 'none' },
                }}
              >
                {pages.map((page) => (
                  <MenuItem key={page} onClick={handleCloseNavMenu}>
                    <Typography textAlign="center">{page}</Typography>
                  </MenuItem>
                ))}
              </Menu>
            </Box> */}
            <img
              // src={`${process.env.PUBLIC_URL ?? ''}/logo.svg`}
              src={logo}
              className="App-Img"
              alt="logo"
            />
            
            <Typography
              className='App-Text'
              noWrap
              component="a"
              onClick={()=>ChangRoute('/')}
              
              sx={{
                display: { xs: 'none', md: 'flex' },
                textDecoration: 'none',
              }}
            >
              智農網
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, justifyContent: 'right' }}>
              {pages.map((page) => (
                
                  <Button
                    startIcon={<img
                      // src={`${process.env.PUBLIC_URL ?? ''}/logo.svg`}
                      src={logo}
                      className="App-Img"
                      alt="logo"
                    />}
                    key={page}
                    onClick={handleCloseNavMenu}
                    className='App-Text'
                  >
                    <Badge color="error" variant="dot" >
                      {page}
                    </Badge>
                  </Button>
                
    
              ))}
            </Box>
            <Typography
              className='App-Text'
              noWrap
              component="a"
              onClick={()=>ChangRoute('/')}
              sx={{            
                display: { xs: 'flex', md: 'none' },            
                textDecoration: 'none',
              }}
            >
              智農網
            </Typography>
            <Box sx={{ flexGrow: { xs: 1, md: 0 }, justifyContent: 'right',  ml: {xs: 0,md:12},textAlign:'right' }} >
              <Tooltip title="Open User Settings" >
                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                  <Avatar>
                    <PersonIcon sx={{fontSize:'36px'}}></PersonIcon>
                  </Avatar>
                </IconButton>
              </Tooltip>
              {/* <Menu
                sx={{ mt: '45px' }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                {settings.map((setting) => (
                  <MenuItem key={setting} onClick={handleCloseUserMenu}>
                    <Typography textAlign="center">{setting}</Typography>
                  </MenuItem>
                ))}
              </Menu> */}
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <Outlet/>
    </Box>
    
  );
};
export default ResponsiveAppBar;
