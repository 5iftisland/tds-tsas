import * as React from 'react';
import { useAppDispatch } from '../../app/hooks';
import {
  Box,
  SwipeableDrawer,
  List,
  Divider,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  IconButton,
  Avatar,
  Typography
} from '@mui/material';
import {useNavigate} from 'react-router-dom'
import {
  Menu,
  SupervisorAccount,
  Reviews,
  Balance,
  GridOn,
  Storefront,
  Apple,
  AirplanemodeActive,
  Announcement,
  Cast,
  BarChart,
  Subtitles,
  Paid,
  ArrowBack
} from '@mui/icons-material';
import PersonIcon from '@mui/icons-material/Person';
import {
  SetToken
} from '../../features/Slice/TokenSlice';
type Anchor = 'top' | 'left' | 'bottom' | 'right';

export default function SwipeableTemporaryDrawer() {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  function ChangRoute(path:string) {
    if(path==='/logout'){
      sessionStorage.removeItem("token");
      dispatch(SetToken(''))
      navigate('/')
    }else {
      navigate(path)
    }
    
  }
  const [listItemName, setListItemName] = React.useState([
    {name:'帳號管理',path: '/Backstage/AccountMent', icon: <SupervisorAccount/>},
    {name:'帳號審查',path: '/Backstage/AccountReviewList', icon: <Reviews/>},
    {name:'角色/權限管理',path: '', icon: <Balance/>},
    {name:'農地管理',path: '', icon: <GridOn/>},
    {name:'作物管理',path: '', icon: <Apple/>},
    {name:'農資材管理"',path: '', icon: <Storefront/>},
    {name:'無人機管理',path: '', icon: <AirplanemodeActive/>},
    {name:'公告管理',path: '', icon: <Announcement/>},
    {name:'推播資訊管理',path: '', icon: <Cast/>},
    {name:'訂單統計',path: '', icon: <BarChart/>},
    {name:'折扣券管理',path: '', icon: <Subtitles/>},
    {name:'金流管理',path: '', icon: <Paid/>},
    {name:'登出',path: '/logout', icon: <ArrowBack/>},
  ]);
  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
    };

  const list = (anchor: Anchor) => (
    <Box
      className='Drawer'
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <Divider />
      <List className='List-content'>
        <ListItem className='List-FirstItem' disablePadding>
          <ListItemButton onClick={()=>ChangRoute('/Backstage')}>
            <Avatar sx={{ m: 0 }}>
              <PersonIcon ></PersonIcon>
            </Avatar>
            <div className='title'>
              智農網
              {/* <div className='sub-title'>
                編輯帳戶資訊
              </div> */}
            </div>
            {/* <div className='status'>
              審核中
            </div> */}
          </ListItemButton>
        </ListItem>
        <Divider />
        {listItemName.map((obj, index) => (
          <ListItem key={obj.name} disablePadding>
            <ListItemButton onClick={()=>ChangRoute(obj.path)}>
              <ListItemIcon>
                {obj.icon}
              </ListItemIcon>
              <ListItemText  primary={obj.name} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <div>
       
          <IconButton onClick={toggleDrawer('left', true)} sx={{color:'#AAAAAA'}}>
            <Menu  sx={{ mr: 1 ,fontSize:'3.6rem'}} />
          </IconButton>
          <SwipeableDrawer
            anchor={'left'}
            open={state['left']}
            onClose={toggleDrawer('left', false)}
            onOpen={toggleDrawer('left', true)}
          >
            {list('left')}
          </SwipeableDrawer>
      
      
    </div>
  );
}
