import {useState, useEffect} from 'react';
import { RESTfulPost } from '../../features/API/RESTfulProcess';
import { useAppDispatch } from '../../app/hooks';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import logo from '../../assets/img/logo512.png';//'./assets/img/logo.svg';
import Badge from '@mui/material/Badge';
import PersonIcon from '@mui/icons-material/Person';
import {useNavigate} from 'react-router-dom';
import Drawer from './BackstageDrawer'
import { useAppSelector } from '../../app/hooks';
import {
  selectToken,SetToken
} from '../../features/Slice/TokenSlice';
import {
    Outlet
} from 'react-router-dom'

const ResponsiveAppBar = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const Token:string = useAppSelector(selectToken);
  console.log(Token)
  
  function ChangRoute(path:string) {
    navigate(path)
  }
  const authorized=async (sessionToken:string)=>{
    if(!sessionToken) {return}
    const graphqlQuery = {
      query: `query  {
        authorized() {
          errorCode
          messages
          succeeded
        }
      }`,
      variables: {}
    };
    const response:any = await RESTfulPost('https://cloudtest.geosat.com.tw:8081/graphql',{Token: sessionToken,Data:graphqlQuery});
    console.log(response)  
    if(!response.errors) {
      if(response.data.authorized.succeeded){
        dispatch(SetToken(sessionToken))
      }else {
        ChangRoute('/')
      }
    }
  }
  useEffect(() => {
    if(!Token) {
      console.log('Token')
      const sessionToken = decodeURIComponent(sessionStorage.getItem('token')||'');
      if(sessionToken) {
        authorized(sessionToken)
      } else {
        ChangRoute('/')
      }
      
    }
    /* 上面是 componentDidMount和componentDidUpdate */
  }, []);
  return (
    <Box
        component="div"
      >
      <AppBar position="static" color='default'  className='App-Bar'>
        <Container maxWidth="xl" >
          <Toolbar disableGutters className='App-Text'>
            <Drawer/>
            
            <img
              // src={`${process.env.PUBLIC_URL ?? ''}/logo.svg`}
              src={logo}
              className="App-Img"
              alt="logo"
            />
            
            <Typography
              className='App-Text'
              noWrap
              component="a"
              onClick={()=>ChangRoute('/Backstage')}
              
              sx={{
                display: { xs: 'none', md: 'flex' },
                textDecoration: 'none',
              }}
            >
              智農網後臺管理系統
            </Typography>
            {/* <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, justifyContent: 'right' }}>
              {pages.map((page) => (
                
                  <Button
                    startIcon={<img
                      // src={`${process.env.PUBLIC_URL ?? ''}/logo.svg`}
                      src={logo}
                      className="App-Img"
                      alt="logo"
                    />}
                    key={page}
                    onClick={handleCloseNavMenu}
                    className='App-Text'
                  >
                    <Badge color="error" variant="dot" >
                      {page}
                    </Badge>
                  </Button>
                
    
              ))}
            </Box> */}
            <Typography
              className='App-Text'
              noWrap
              component="a"
              onClick={()=>ChangRoute('/')}
              sx={{            
                display: { xs: 'flex', md: 'none' },            
                textDecoration: 'none',
              }}
            >
              智農網後臺管理系統
            </Typography>
            {/* <Box sx={{ flexGrow: { xs: 1, md: 0 }, justifyContent: 'right',  ml: {xs: 0,md:12},textAlign:'right' }} >
              <Tooltip title="Open User Settings" >
                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                  <Avatar>
                    <PersonIcon sx={{fontSize:'36px'}}></PersonIcon>
                  </Avatar>
                </IconButton>
              </Tooltip>
        
            </Box> */}
          </Toolbar>
        </Container>
      </AppBar>
      <Outlet/>
    </Box>
    
  );
};
export default ResponsiveAppBar;
