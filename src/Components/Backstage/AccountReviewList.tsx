import {useState, useEffect}  from 'react';
import { useAppSelector,useAppDispatch } from '../../app/hooks';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {useNavigate} from 'react-router-dom';
import {EnhancedTable,HeadCell} from '../Table/Table'
import AccountReview from './AccountReview'
import { RESTfulPost } from '../../features/API/RESTfulProcess';
import {AccountReviewTableRow} from '../../assets/config/interface';
import {userRoleAudit} from '../../assets/config/interface';
import {
  selectToken,SetToken
} from '../../features/Slice/TokenSlice';
import {
  selectDialog,
  SetDialog,
} from '../../features/Slice/DialogSlice';
const AccountReviewList=  () => {
const dispatch = useAppDispatch();
const navigate = useNavigate();
function createData(
  id: string,
  displayName: string|null,
  Name: string,
  email: string|null,
  phoneNumber: string|null, 
  description: string|null, 
  // select: string,
  edit: string,
  checkbox : boolean
): AccountReviewTableRow {
  return {
    id,
    displayName,
    Name,
    email,
    phoneNumber,
    description,
    // select,
    edit,
    checkbox
  };
}
const [ReviewUserList, setReviewUserList] = useState([]);
const [Data, setData] = useState([]);
const [SelectedUser, setSelectedUser] = useState(null);
const Token:string = useAppSelector(selectToken);
const BtnOnClick =(obj: AccountReviewTableRow)=>{
  handleOpen()
  setSelectedUser(null)
  const item = ReviewUserList.find((element:userRoleAudit)=>{
    return element.user.id===obj.id
  })
  if(item) {
    setSelectedUser(item)
  }
  console.log(obj)
  
}
function ChangRoute(path:string) {
    navigate(path)
  }
// let Data:Array<userRoleAudit> = []
const getRevewList=async ()=>{
  console.log('getRevewList')
  if(!Token) {ChangRoute('/Backstage'); return}
  const graphqlQuery = {
    query: `query fetchAuthor {
      userRoleAudits(type: REVIEW) {
        type
        comment
        isPaid
        createdOn
        reviewOn
        user {
          id displayName lastName firstName  email phoneNumber 
          userProfileExtendedAttributeResponse {
            bandCode
            bankAccount
            bankbookUrl
            licenseCode
            licenseUrl
          }
        }
        role {
          id
          description
        }
      }
    }`,
    variables: {}
  };
  const response:any = await RESTfulPost('https://cloudtest.geosat.com.tw:8081/graphql',{Token: Token,Data:graphqlQuery});
  console.log(response)  
  if(!response.errors) {
    setReviewUserList(response.data.userRoleAudits)
    // createData('Cupcake', 305, 3.7, 67, 4.3,'審查',false)
    const array =response.data.userRoleAudits.map((element:userRoleAudit)=>{
      return createData(element.user.id, element.user.displayName, `${element.user.firstName}${element.user.lastName}`, element.user.email, element.user.phoneNumber, element.role.description,'審查',false)
    })
       setData(array)
  }
}
 const handleOpen = () => {
    // props.UpDate('CropDialog','updata');
    dispatch(SetDialog(true))
  };
 useEffect(() => {
    getRevewList()
  },[]);
  const headCells:  HeadCell[] = [
    // {
    //   id: 'checkbox',
    //   numeric: false,
    //   disablePadding: false,
    //   label: '',
    //   type: 'checkbox'
    // },
    // {
    //   id: 'id',
    //   align: 'center',
    //   disablePadding: false,
    //   label: '',
    //   display:false
    // },
    {
      id: 'displayName',
      align: 'center',
      disablePadding: false,
      label: '申請人名稱',
    },
    {
      id: 'Name',
      align: 'center',
      disablePadding: false,
      label: '真實姓名',
    },
    {
      id: 'email',
      align: 'center',
      disablePadding: false,
      label: '電子信箱',
    },
    {
      id: 'description',
      align: 'center',
      disablePadding: false,
      label: '申請會員階級',
    },
    {
      id: 'phoneNumber',
      align: 'center',
      disablePadding: false,
      label: '手機號碼',
    },
    // {
    //   id: 'select',
    //   align: 'center',
    //   disablePadding: false,
    //   label: 'Protein (g)',
    //   type: 'select',
    //   items: ['啟用','停用'],
    //   function: SelectOnCheng
    // },
    {
      id: 'edit',
      align: 'center',
      disablePadding: false,
      label: '功能',
      type: 'button',
      function: BtnOnClick,
    },
  ];
  
  return (
    <Box className='tbale' sx={{ width: '100%' }}>
      <Typography
        className='title'
        component="span"     
      >
        帳號審查清單
      </Typography> 
      <EnhancedTable TableHead={headCells} TableData={Data}/>
      <AccountReview ReviewUser={SelectedUser} getRevewList={getRevewList}></AccountReview>
    </Box>
  );
}

export default AccountReviewList
