import {useNavigate} from 'react-router-dom'
import  '../../assets/scss/Backstage.scss'
import {
    Grid,
} from '@mui/material';

const Index = () => {
    const navigate = useNavigate();
    const Items=[
        {name:{__html:"帳號<br/>管理"}, class: 'hex-2',path: ''},
        {name:{__html:"帳號<br/>審查"}, class: 'hex-1',path: '/Backstage/AccountReviewList'},
        {name:{__html:"角色/權限<br/>管理"}, class: 'hex-2',path: ''},
        {name:{__html:"農地<br/>管理"}, class: 'hex-1',path: ''},
        {name:{__html:"作物<br/>管理"}, class: 'hex-2',path: ''},
        {name:{__html:"農資材<br/>管理"}, class: 'hex-1',path: ''},
        {name:{__html:"無人機<br/>管理"}, class: 'hex-2',path: ''},
        {name:{__html:"公告<br/>管理"}, class: 'hex-1',path: ''},
        {name:{__html:"推播資訊<br/>管理"}, class: 'hex-2',path: ''},
        {name:{__html:"訂單<br/>統計"}, class: 'hex-1',path: ''},
        {name:{__html:"折扣券<br/>管理"}, class: 'hex-2',path: ''},
        {name:{__html:"金流<br/>管理"}, class: 'hex-1',path: ''},
      
    ]
    function ChangRoute(path:string) {
        navigate(path)
    }
    return (
      <div className='backstage-index'>
        <div style={{width:'500px'}}>
            {Items.map((obj,index) => (
                <div className={`hex ${obj.class}  ${(index)%5===0?'hex-gap':''}`} onClick={()=>ChangRoute(obj.path)}>
                    <span  dangerouslySetInnerHTML={obj.name}></span>
                    <div className="corner-1"></div>
                    <div className="corner-2"></div>
                </div>
            ))}
        
        </div>
        
      </div>
      
    );
}


export default Index;
