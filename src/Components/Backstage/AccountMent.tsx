import {useState}  from 'react';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import {EnhancedTable,HeadCell} from '../Table/Table'
import {AccountTableRow} from '../../assets/config/interface';


export default function AccountMent() {
  


function createData(
  name: string,
  calories: number,
  fat: number,
  carbs: number,
  protein: number,
  select: string,
  edit: string,
  checkbox : boolean
): AccountTableRow {
  return {
    name,
    calories,
    fat,
    carbs,
    protein,
    select,
    edit,
    checkbox
  };
}

const BtnOnClick =()=>{
  console.log('123')
}
const [Data, setData] = useState([
  createData('Cupcake1', 305, 3.7, 67, 4.3,'啟用','編輯',false),
  // createData('Donut', 452, 25.0, 51, 4.9,'啟用','編輯',false),
  // createData('Eclair', 262, 16.0, 24, 6.0,'啟用','編輯',false),
  // createData('Frozen yoghurt', 159, 6.0, 24, 4.0,'啟用','編輯',false),
  // createData('Gingerbread', 356, 16.0, 49, 3.9,'啟用','編輯',false),
  // createData('Honeycomb', 408, 3.2, 87, 6.5,'啟用','編輯',false),
  // createData('Ice cream sandwich', 237, 9.0, 37, 4.3,'啟用','編輯',false),
  // createData('Jelly Bean', 375, 0.0, 94, 0.0,'啟用','編輯',false),
  // createData('KitKat', 518, 26.0, 65, 7.0,'啟用','編輯',false),
  // createData('Lollipop', 392, 0.2, 98, 0.0,'啟用','編輯',false),
  // createData('Marshmallow', 318, 0, 81, 2.0,'啟用','編輯',false),
  // createData('Nougat', 360, 19.0, 9, 37.0,'啟用','編輯',false),
  // createData('Oreo', 437, 18.0, 63, 4.0,'啟用','編輯',false),
]);

  
  const SelectOnCheng =(event: React.ChangeEvent<HTMLInputElement>, name:string)=>{
    setData(Data.map((obj,j)=>{
    
      if(obj.name===name)
        obj.select = event.target.value; 
      return obj
    }))
    // console.log(event.target.value);
    // // Data[index].select = event.target.value;
    // console.log(event.target.value);
  }
  const headCells:  HeadCell[] = [
    // {
    //   id: 'checkbox',
    //   numeric: false,
    //   disablePadding: false,
    //   label: '',
    //   type: 'checkbox'
    // },
    {
      id: 'name',
      align: 'center',
      disablePadding: false,
      label: 'Dessert (100g serving)',
    },
    {
      id: 'calories',
      align: 'center',
      disablePadding: false,
      label: 'Calories',
    },
    {
      id: 'fat',
      align: 'center',
      disablePadding: false,
      label: 'Fat (g)',
    },
    {
      id: 'carbs',
      align: 'center',
      disablePadding: false,
      label: 'Carbs (g)',
    },
    {
      id: 'protein',
      align: 'center',
      disablePadding: false,
      label: 'Protein (g)',
    },
    {
      id: 'select',
      align: 'center',
      disablePadding: false,
      label: 'Protein (g)',
      type: 'select',
      items: ['啟用','停用'],
      function: SelectOnCheng
    },
    {
      id: 'edit',
      align: 'center',
      disablePadding: false,
      label: '功能',
      type: 'button',
      function: BtnOnClick
    },
  ];
  
  return (
    <Box className='tbale' sx={{ width: '100%' }}>
      <Typography
        className='title'
        component="span"     
      >
        帳號管理
      </Typography> 
      <EnhancedTable TableHead={headCells} TableData={Data}/>
    </Box>
  );
}
