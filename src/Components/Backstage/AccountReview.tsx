import {useState, useEffect}  from 'react';
import { useAppSelector,useAppDispatch } from '../../app/hooks';

import {useNavigate} from 'react-router-dom';
import {Typography,List,Box,TextField,ListItem,ListItemText,Card,RadioGroup,CardMedia,Button,Grid,Breadcrumbs,Link,DialogTitle,Dialog  } from '@mui/material';
import { RESTfulPost } from '../../features/API/RESTfulProcess';
import {AccountReviewTableRow} from '../../assets/config/interface';
import {userRoleAudit} from '../../assets/config/interface';
import {
  selectToken,SetToken
} from '../../features/Slice/TokenSlice';
import  { DefaultSettingsT, SettingsT } from '../../Components/Index/Settings';
import {
  selectDialog,
  SetDialog,
} from '../../features/Slice/DialogSlice';
import Carousel from 'react-material-ui-carousel';
import '../../assets/scss/Text.scss'
import '../../assets/scss/Dispaly.scss'
interface Prop {
  ReviewUser:userRoleAudit|null
  getRevewList: Function
}
interface Item  {
    Name: string;
    Image: string;
    // Caption: string,
    // contentPosition: "left" | "right" | "middle",
    // Items: {Name: string, Image: string}[]
}
interface ReviewInfo{
  'displayName': string|null;
      'Name': string|null;
      'phoneNumber':  string|null;
      'email':  string|null;
      'roleDescription':  string|null;
      'bandCode':  string|null;
      'bankAccount':  string|null;
      'licenseCode':  string|null;
}
const AccountReview=  (props: Prop) => {
  console.log(props.ReviewUser)
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [settings, setSettings] = useState<SettingsT>(DefaultSettingsT);
  const [comment, setComment] = useState<string>('');
  const [value, setValue] = useState<ReviewInfo>({
    'displayName': '',
      'Name': '',
      'phoneNumber':  '',
      'email':  '',
      'roleDescription':  '',
      'bandCode':  '',
      'bankAccount':  '',
      'licenseCode':  ''});
  const [items, setItems] = useState<Item[]>([
  {
    Name: "存摺封面",
    Image:  ''
  },
  {
    Name: "證照",
    Image:  ''
  }
]);
  const Token:string = useAppSelector(selectToken);
  const status = useAppSelector(selectDialog);
  const definitions=[
    {id:'displayName', name: '會員名稱',type: 'label'},
    {id:'Name', name: '真實姓名',type: 'label'},
    {id:'phoneNumber', name: '手機號碼',type: 'label'},
    {id:'email', name: '電子郵件',type: 'label'},
    {id:'roleDescription', name: '申請會員階級',type: 'label'},
    {id:'bandCode', name: '銀行代碼',type: 'label'},
    {id:'bankAccount', name: '匯款帳號',type: 'label'},
    {id:'licenseCode', name: '證照號碼',type: 'label'},
  ]
  let ReviewUser=props.ReviewUser
  
  
interface BannerProps
{
    item: Item,
    length?: number,
}
 const Banner = (props: BannerProps) => {

    const item = props.item;
    const media = (
      <Grid item xs={12} key={item.Name}>
        <CardMedia
          className="Media"
          image={item.Image}
          title={item.Name}
        >
          <Typography className="MediaCaption">
            {item.Name}
          </Typography>
        </CardMedia>
      </Grid>
    )

    return (
        <Card raised className="Banner">
            <Grid container spacing={0} className="BannerGrid">
                {media}
            </Grid>
        </Card>
    )
}

  const handleClose = () => {
    // props.UpDate('CropDialog','updata');
    dispatch(SetDialog(false))
    props.getRevewList()
  };
  
  const SendReview=async(key:string)=>{
    const graphqlMutation = {
      query: `mutation {
        userRoleAuditSave(userId: "${ReviewUser?ReviewUser.user.id:''}",roleId: "${ReviewUser?ReviewUser.role.id:''}",type: ${key},comment: "${comment}") {
          errorCode
          messages
          succeeded
        }
      }`,
      variables: {}
    };
    console.log(graphqlMutation)
    const response:any = await RESTfulPost('https://cloudtest.geosat.com.tw:8081/graphql',{Token: Token,Data:graphqlMutation});
    console.log(response)  
    if(!response.errors) {
      if(response.data.userRoleAuditSave.succeeded){
        handleClose()
      }
    }
  }
  useEffect(() => {
    ReviewUser=props.ReviewUser
    setItems( [
      {
        Name: "存摺封面",
        Image: ReviewUser?ReviewUser.user.userProfileExtendedAttributeResponse.bankbookUrl: ''
      },
      {
        Name: "證照",
        Image: ReviewUser?ReviewUser.user.userProfileExtendedAttributeResponse.licenseUrl: ''
      }
    ])
    setValue({
      'displayName': ReviewUser?ReviewUser.user.displayName: '',
      'Name': ReviewUser?`${ReviewUser.user.firstName}${ReviewUser.user.lastName}`:'',
      'phoneNumber':  ReviewUser?ReviewUser.user.phoneNumber: '',
      'email':  ReviewUser?ReviewUser.user.email: '',
      'roleDescription':  ReviewUser?ReviewUser.role.description: '',
      'bandCode':  ReviewUser?ReviewUser.user.userProfileExtendedAttributeResponse.bandCode: '',
      'bankAccount':  ReviewUser?ReviewUser.user.userProfileExtendedAttributeResponse.bankAccount: '',
      'licenseCode':  ReviewUser?ReviewUser.user.userProfileExtendedAttributeResponse.licenseCode: '',
    });
    /* 上面是 componentDidMount和componentDidUpdate */
  }, [props.ReviewUser]);
  return (
    <Dialog
        fullWidth={true}
        maxWidth={'xl'}
        open={status}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle className='Dialog-Title-text'>帳號審查</DialogTitle>
        <Box  sx={{ width: '90%' }} className='page-content'>
          {/* <Typography
            className='title'
            component="span"     
          >
            帳號審查
          </Typography>  */}
          {/* <Breadcrumbs aria-label="breadcrumb" className='breadcrumbs-text'>
            <Link underline="hover" color="inherit" onClick={()=>ChangRoute('/Backstage')}>
              首頁
            </Link>
            <Link
              underline="hover"
              color="inherit"
              onClick={()=>ChangRoute('/Backstage/AccountReviewList')}
            >
              列表
            </Link>
            <Typography color="text.primary">帳號審查</Typography>
          </Breadcrumbs> */}
          <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }} className='dispaly-flex'>
            <Grid item  xs={12} sm={6} md={4} className='dispaly-flex-item'>
              
              <Box
                component="div"
              >
                <List
                  component="div"
                >
                  {definitions.map((e)=>(
                    <ListItem key={e.name} >
                      <ListItemText >
                        <span className={'list-item-text'}>{e.name}:</span>
                        <Typography
                          sx={{ml:5}}
                          className={'list-item-text'}
                          component="span"     
                        >
                          {value[e.id as keyof typeof value]}
                        </Typography>
                      </ListItemText>   
                    </ListItem>
                  ))}
                </List>
              </Box>
            </Grid>
            <Grid item  xs={12} sm={6} md={6} className='dispaly-flex-item flex-container-column'>
              <Carousel
                  {...settings}
                >
                  {
                    items.map((item, index) => {
                      return <Banner item={item} key={index}  />
                    })
                  }
                </Carousel>
              
            </Grid>
          </Grid>
        
          
          <TextField
            sx={{ width: '100%' }}
            className='TextField-text'
              label="審查意見"
              multiline
              rows={4}
              variant="standard"
              onChange={(e) => setComment(e.target.value)}
            />
            <Button variant="contained"  sx={{ width: '49%', mt:2, mb:2, mr:2,fontSize:'1.8rem',color:'rgb(250, 250, 250)' }} onClick={() => SendReview('DENIED')}>通過</Button>
            <Button variant="contained"  sx={{ width: '49%', mt:2, mb:2,fontSize:'1.8rem',color:'rgb(250, 250, 250)', backgroundColor:'rgb(196, 37, 37)'  }} onClick={() => SendReview('PASSED')}>不通過</Button>
        </Box>
      </Dialog>
    
  );
}
export default AccountReview
