import {useState}  from 'react';
import { useAppDispatch } from '../../app/hooks';
import {Fab,List,Box,IconButton,ListItem,ListItemText,TextField,Select,MenuItem,Button } from '@mui/material';
import CropCard from '../Crad/CropCard';
import CropDialog from '../Dialog/CropDialog'

import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import {
  SetDialog,
} from '../../features/Slice/DialogSlice';
const Info = () => {
  const dispatch = useAppDispatch();
  const [items, setItems] = useState(
    [
      {species: '1',Variety: '',Certification: '' },
      {species: '2',Variety: '',Certification: '' },
      {species: '3',Variety: '',Certification: '' },
      {species: '4',Variety: '',Certification: '' },
      {species: '5',Variety: '',Certification: '' },
      {species: '6',Variety: '',Certification: '' },
      {species: '7',Variety: '',Certification: '' },
      {species: '8',Variety: '',Certification: '' },
      {species: '9',Variety: '',Certification: '' },
    ]
  );
  const [model, setModel] = useState('new')
  const UpDate=(source:string,model: string, value:string)=>{
    // 更新資料用
    console.log(source)
    switch(model) {
      case'updata':
        setItems([
          {species: '1',Variety: '',Certification: '' },
          {species: '2',Variety: '',Certification: '' }
        ])
      break
      case 'setModel':
        setModel(value)
        break
    }
  }
  const FabClick=()=>{
    setModel('new')
    dispatch(SetDialog(true))
  }
  return (
    <div className='crop-content'>
      {items.map((e)=>(
        <CropCard UpDate={UpDate} key={e.species} />
      ))}
      <Fab color="primary" className='Fab-btn' onClick={() => FabClick()}>
        <AddIcon sx={{fontSize: '2rem',color:'#FFF'}}/>
      </Fab>
      <CropDialog UpDate={UpDate} Model={model}/>
    </div>
  );
};
export default Info;


