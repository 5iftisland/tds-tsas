import {useState, forwardRef} from 'react';
import { useAppSelector, useAppDispatch } from '../../app/hooks';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import {Typography,List,FormControl,RadioGroup,FormControlLabel,Radio,ListItem,ListItemText,TextField,Select,MenuItem,Button,FormGroup,Checkbox } from '@mui/material';
import { TransitionProps } from '@mui/material/transitions';
import {
  selectDialog,
  SetDialog,
} from '../../features/Slice/DialogSlice';
import {FarmlandDefinition} from '../../assets/config/interface';

const Transition = forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function CropCard(props:Prop) {
  const Model =props.Model
  const definitions=[
    {id:'1', name: '縣市　　',type: 'select',items:[{label:'台南市',value: '台南市'}]},
    {id:'2', name: '行政區　',type: 'select',items:[{label:'東區',value: '東區'}]},
    {id:'3', name: '地段　　',type: 'select',items:[{label:'XX段',value: 'XX段'}]},
    {id:'4', name: '地號　　',type: 'text',},
    {id:'6', name: '座標　　',type: 'text',},
    {id:'7', name: '面積　　',type: 'text',},
    {id:'8', name: '坡地　　',type: 'Radio', items:[{label:'有',value: '有'},{label:'無',value: '無'}]},
    {id:'9', name: '行動網路',type: 'Radio', items:[{label:'有',value: '有'},{label:'無',value: '無'}]},
    {id:'10', name: '障礙物　',type: 'Checkbox', items:[{label:'鄰田障礙物',value: '鄰田障礙物'},{label:'捕鳥網',value: '捕鳥網'},{label:'高壓電塔',value: '高壓電塔'},{label:'電線',value: '電線'},{label:'廣告板',value: '廣告板'},{label:'其他',value: '其他'}]},
  ]
  const [value, setValue] = useState({
    '1': '台南市',
    '2': '東區',
    '3': 'XX段',
    '4': '',
    '5': '',
    '6': '',
    '7': '',
    '8': '',
    '9': '',
    '10': ',有'
  });
  const status = useAppSelector(selectDialog);
  const dispatch = useAppDispatch();
  
  const handleClose = () => {
    // props.UpDate('CropDialog','updata');
    dispatch(SetDialog(false))
  };

  function Greeting(props:FarmlandDefinition) {
    const id = props.id;
    const text = props.text;
    const items = props.items;
    const type = Model==='new'||Model==='edit'?props.type:'label';
    switch(type){
       case 'select':
        
        return <Select
          sx={{ml:5,fontSize:'1.8rem'}}
          value={text}           
          onChange={((event)=>{
            // value[id]=event.target.value;
            setValue(prevState  => { 
              let updatedValues=prevState ; 
              updatedValues[id as keyof typeof value]=event.target.value;  
              return {...prevState, ...updatedValues};}
            );
          })}    
        >
          {items.map((e)=>(<MenuItem key={e.label} sx={{fontSize:'1.5rem'}} value={e.value}>{e.label}</MenuItem>))}
        </Select>
  
      case 'label':
        return  <Typography
                sx={{ml:5}}
                // className='info-title'
                component="span"     
              >
                {text}
              </Typography>;
      case 'Radio': 
        return <FormControl sx={{ml:5}}>
          <RadioGroup
            value={text}
            
            onChange={((event)=>{
            // value[id]=event.target.value;
            setValue(prevState  => { 
              let updatedValues=prevState ; 
              updatedValues[id as keyof typeof value]=event.target.value;  
              return {...prevState, ...updatedValues};}
            );
          })}  
          >
            {items.map((e)=>(<FormControlLabel key={e.label} value={e.value} control={<Radio />} label={e.label} />))}
          </RadioGroup>
        </FormControl>
      case 'Checkbox':
        return  <FormControl sx={{ml:5}}>
                  {items.map((e)=>(<FormControlLabel key={e.label} value={e.value} control={
                  <Checkbox 
                    checked={value[id as keyof typeof value].indexOf(e.value)>-1}
                    onChange={((event)=>{
                      // value[id]=event.target.value;
                      setValue(prevState  => { 
                        let updatedValues=prevState ;
                        console.log(prevState[id as keyof typeof value].indexOf(event.target.value)) 
                        if(prevState[id as keyof typeof value].indexOf(event.target.value)>-1) {
                          updatedValues[id as keyof typeof value]=updatedValues[id as keyof typeof value].replace(','+event.target.value,'')
                        } else {
                          updatedValues[id as keyof typeof value]+=','+event.target.value
                        }
                       
                        console.log(updatedValues[id as keyof typeof value]) 
                       
                        return {...prevState, ...updatedValues};}
                      );
                    })}  
                  />} label={e.label} />))}          
                </FormControl>
      default:
        return <TextField
              sx={{ml:5}}
              required
              // onChange={(e) => setCode(e.target.value)}
              className='textField'
              type="password"
            />
    }

  }
  const GetModelName= ()=>{
    switch(Model){
      case 'new':
        return '新增'
      case 'edit':
        return '修改'
      case 'delete':
        return '刪除'
      case 'info':
        return '詳細'
    }
  }
  return (
   
      <Dialog
        className='dialog'
        maxWidth={'xl'}
        open={status}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle className='dialog-title'>{`${GetModelName()}農地資料`}</DialogTitle>
        <DialogContent >
          <List
              component="div"
            >
              {definitions.map((e)=>(
                <ListItem key={e.name}>
                  <ListItemText >{e.name}{<Greeting  text={value[e.id as keyof typeof value]} type={e.type} items={e.items||[]} id={e.id} />}  </ListItemText>   
                </ListItem>
              ))}
            </List>
        </DialogContent>
        <DialogActions>
          {/* <Button className='Cancel-btn' onClick={handleClose}>取消</Button> */}
          
          {Model==='info'?'':<Button className='Success-btn-small' >{GetModelName()}</Button>}
        </DialogActions>
      </Dialog>
    
  );
}
interface Prop {
  UpDate: Function 
  Model?: string
 
}

