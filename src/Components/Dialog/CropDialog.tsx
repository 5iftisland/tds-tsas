import {useState, forwardRef} from 'react';
import { useAppSelector, useAppDispatch } from '../../app/hooks';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import {Typography,List,Box,IconButton,ListItem,ListItemText,TextField,Select,MenuItem,Button } from '@mui/material';
import { TransitionProps } from '@mui/material/transitions';
import {
  selectDialog,
  SetDialog,
} from '../../features/Slice/DialogSlice';

import {CropDefinition} from '../../assets/config/interface';

const Transition = forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function CropCard(props:Prop) {
  const Model =props.Model
  const definitions=[
    {id:'1', name: '物種名稱',type: 'select',items:[{label:'台梗九號',value: '台梗九號'},{label:'台梗九號',value: '台梗九號'}]},
    {id:'2', name: '品種名稱',type: 'select',items:[{label:'台梗九號',value: '台梗九號'},{label:'台梗九號',value: '台梗九號'}]},
    {id:'3', name: '認證類型',type: 'select',items:[{label:'產銷履歷',value: '產銷履歷'},{label:'有機',value: '有機'},{label:'無',value: '無'}]},
  ]
  const [value, setValue] = useState({
    '1': '台梗九號',
    '2': '台梗九號',
    '3': '產銷履歷',
  });
  const status = useAppSelector(selectDialog);
  const dispatch = useAppDispatch();
  
  const handleClose = () => {
    // props.UpDate('CropDialog','updata');
    dispatch(SetDialog(false))
  };

  function Greeting(props:CropDefinition) {
    const id = props.id;
    const text = props.text;
    const type = Model==='new'||Model==='edit'?props.type:'label';
    switch(type){
       case 'select':
        const items = props.items;
        
        return <Select
          sx={{ml:5,fontSize:'1.8rem'}}
          value={text}           
          onChange={((event)=>{
            // value[id]=event.target.value;
            setValue(prevState  => { 
              let updatedValues=prevState ; 
              updatedValues[id as keyof typeof value]=event.target.value;  
              return {...prevState, ...updatedValues};}
            );
          })}    
        >
          {items.map((e)=>(<MenuItem key={e.label} sx={{fontSize:'1.5rem'}} value={e.value}>{e.label}</MenuItem>))}
        </Select>
      case 'label':
        return  <Typography
                sx={{ml:5}}
                // className='info-title'
                component="span"     
              >
                {text}
              </Typography>;
      default:
        return <TextField
              sx={{ml:5}}
              required
              // onChange={(e) => setCode(e.target.value)}
              className='textField'
              type="password"
            />
    }

  }
  const GetModelName= ()=>{
    switch(Model){
      case 'new':
        return '新增'
      case 'edit':
        return '修改'
      case 'delete':
        return '刪除'
      case 'info':
        return '詳細'
    }
  }
  return (
      <Dialog
        className='dialog'
        maxWidth={'xl'}
        open={status}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle className='dialog-title'>{`${GetModelName()}作物資料`}</DialogTitle>
        <DialogContent >
          <List
              component="div"
            >
              {definitions.map((e)=>(
                <ListItem key={e.name}>
                  <ListItemText >{e.name}{<Greeting  text={value[e.id as keyof typeof value]} type={e.type} items={e.items||[]} id={e.id} />}  </ListItemText>   
                </ListItem>
              ))}
            </List>
        </DialogContent>
        <DialogActions>
          {/* <Button className='Cancel-btn' onClick={handleClose}>取消</Button> */}
          
          {Model==='info'?'':<Button className='Success-btn-small' >{GetModelName()}</Button>}
        </DialogActions>
      </Dialog>
    
  );
}
interface Prop {
  UpDate: Function 
  Model?: string
}

