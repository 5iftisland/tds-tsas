import * as React from 'react';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import {
  selectImgDialog,
  SetImgDialog,
} from '../../features/Slice/ImgDialogSlice';
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function ImgDialog(props:Prop) {
  const path = props.path;
  const status = useAppSelector(selectImgDialog);
  const dispatch = useAppDispatch();
  // const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    dispatch(SetImgDialog(false))
  };

  return (
   
      <Dialog
        
        maxWidth={'xl'}
        open={status}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        {/* <DialogTitle>{"Use Google's location service?"}</DialogTitle> */}
        <DialogContent >
          <img
            // src={`${process.env.PUBLIC_URL ?? ''}/logo.svg`}
            src={path}
            
            alt="沒有圖片"
          />
        </DialogContent>
        {/* <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button onClick={handleClose}>Agree</Button>
        </DialogActions> */}
      </Dialog>
    
  );
}
interface Prop {
  path: string,
 
}
