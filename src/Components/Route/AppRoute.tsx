import React from 'react'
import Index from '../index'
import Login from '../Account/Login'
import Register from '../Account/Register'
import VerifyMode from '../Account/VerifyMode'
import HeadBar from '../Bar/HeadBar'
import BackstageHeadBar from '../Bar/BackstageHeadBar'
import AccountTabs from '../Tabs/AccountTabs'
import Information from '../Account/Information'
import Upgrade from '../Account/Upgrade'
import ResetPassword from '../Account/ResetPassword'
import Review from '../Account/AccountUpgrade'
import FarmTabs from '../Tabs/FarmTabs'
import Crop from '../Farm/Crop'
import Materials from '../Farm/Materials'
import Farmland from '../Farm/Farmland'
import BackstageIndex from '../Backstage/BackstageIndex'
import AccountMent from '../Backstage/AccountMent'
import AccountReviewList from '../Backstage/AccountReviewList'
import AccountReview from '../Backstage/AccountReview'

import {
    // BrowserRouter as Router,
    HashRouter as Router,
    // Routes,
    // Route,
    Link,
    useNavigate,
    Outlet,
    useLocation,
    useParams,
    useRoutes
} from 'react-router-dom'
// Router多層，Outlet輸出下層的DOM
// useLocation 抓取網址中的資訊
// useParams 抓取動態參數 （ex:http://localhost:3000/list/child2/666）
// useSearchParams 取得網址參數，也可以這樣運用 const [searchParams,setSearchParams]= useSearchParams();
// path='*' No Match！如果網址沒有 match 的話
// useNavigate 的第一個參數可以是路由或是數字，代表前進或回去的頁數。
// const Home = () => {
//     return (
//         <div>
//             <ul>
//                 <li>HOME</li>
//                 <li>
//                     <Link to='/about'>About</Link>
//                 </li>
//                 <li>
//                     <Link to='/list'>List</Link>
//                 </li>
//             </ul>
//         </div>
//     )
// }
// const About = () => {
//     const navigate = useNavigate()
//     const onClick = () => {
//         navigate('/')
//     }
//     return (
//         <div>
//           <button onClick={onClick}>BACK</button>
//         </div>
//     )
// }
// const ProfilePage = () => {
//     const location = useLocation()
//     console.log(location)
//     const params = useParams()
//     console.log(params, 'params')
//     return <div>{params.userId}</div>
// }

// const Child1 = () => {
//     const location = useLocation()
//     console.log(location)
//     const params = useParams()
//     console.log(params, 'params')
//     return (
//     <div>
//       Child1
//       <Outlet />
//     </div>)
// }
// const Child2 = () => {
//     const location = useLocation()
//     console.log(location)
//     return <div>Child2</div>
// }

// const List = () => {
//     return (
//         <div>
//             list 頁面
//             <Menus1 />
//             {/* Router多層，Outlet輸出下層的DOM*/}
//             <Outlet />
//         </div>
//     )
// }

// const Menus1 = () => {
//   const navigate = useNavigate()
//   const onClick = () => {
//     navigate('/')
//   }
//   return (     
//     <div>
//       <Link to={'/list/child1'}> one </Link>
//       <Link to={'/list/child1/123'}> one </Link>
//       <Link to={'/list/child2'}> two </Link>
//       <button onClick={onClick}>BACK</button>
//     </div>
//   )
// }

const routeConfig = [
    {
        path: '/',
        element: <HeadBar />,
        children: [
           { path: '/', element: <Index /> },
           { path: '/login', element: <Login /> },
           { path: '/Register', element: <Register /> },
           { path: '/VerifyMode', element: <VerifyMode /> },
           {
                path: '/Account',
                element: <AccountTabs />,
                children: [
                    { path: '/Account/Information', element: <Information /> },
                    { path: '/Account/Upgrade', element: <Upgrade /> },
                    { path: '/Account/ResetPassword', element: <ResetPassword /> },
                    { path: '/Account/Review', element: <Review /> },
                ],
            },
            {
                path: '/Farm',
                element: <FarmTabs />,
                children: [
                    { path: '/Farm/Crop', element: <Crop /> },
                    { path: '/Farm/Farmland', element: <Farmland /> },
                    { path: '/Farm/Materials', element: <Materials /> },
                    { path: '/Farm/Review', element: <Review /> },
                ],
            },
        ]
    },
    {
        path: '/Backstage',
        element: <BackstageHeadBar />,
        children: [
           { path: '/Backstage', element: <BackstageIndex /> },
           { path: '/Backstage/AccountMent', element: <AccountMent /> },
           { path: '/Backstage/AccountReviewList', element: <AccountReviewList /> },
        ]
    }
    // {
    //     path: '/login',
    //     element: <Login />,
    // },
    // {
    //     path: '/Register',
    //     element: <Register />,
    // },
    // {
    //     path: '/VerifyMode',
    //     element: <VerifyMode />,
    // },
    
    
    // { path: '/about', element: <About /> },
    // {
    //     path: '/list',
    //     element: <List />,
    //     children: [
    //         { path: '/list/child1', element: <Child1 />, 
    //           children:[{ path: ':userId', element: <ProfilePage /> }] 
    //         },
    //         { path: '/list/child2', element: <Child2 /> },
    //     ],
    // },
    // { path: '*', element: 
    //   <main style={{ padding: '1rem' }}>
    //     <p>There's nothing here!</p>
    //   </main> 
    // },
]
const AppRoute = () => {
    const element = useRoutes(routeConfig)
    return (
        <div className='page'>
            <div className='content'>{element}</div>
        </div>
    )
}

export function App() {
  return (
        <Router>
           
            <AppRoute/>
          {/* <Routes>
            <Route element={<Home />} path={'/'}></Route>
            <Route element={<About />} path='/about'></Route>
            <Route element={<List />} path='/list'>
              <Route element={<Child1 />} path='/list/child1'>
                <Route path=":userId" element={<ProfilePage />} />
              </Route>
              <Route element={<Child2 />} path='/list/child2'></Route>
            </Route>
            <Route
              path='*'
              element={
                <main style={{ padding: '1rem' }}>
                      <p>There's nothing here!</p>
                </main>
                }
            />
          </Routes> */}
        </Router>
  )
}

export default App
