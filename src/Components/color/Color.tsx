import { createTheme } from '@mui/material/styles';


export const theme = createTheme({
  
  palette: {
   
    primary: {
      light: '#ff7961',
      main: '#1A8A80',
      dark: '#AAAAAA',
      contrastText: '#000',
    },
  },
});


