import {useState} from 'react';
import {Typography,Box,TextField,Button,Divider,Link} from '@mui/material';
import {useNavigate} from 'react-router-dom'

export default function CountrySelect() {
  const [Phone, setPhone] = useState('');
  const [Code, setCode] = useState('');
  const navigate = useNavigate();
  function ChangRoute(path:string) {
    navigate(path)
  }
 
  return (
    <div className="Base">
      <Typography
        className='title'
        component="span"     
        sx={{
          fontWeight: 700,
        }}
      >
        註冊
      </Typography> 
      
      <Box
        className='Register-content'
        component="form"
        // sx={{
        //   '& .MuiTextField-root': { m: 1, width: '25ch' },
        // }}
        noValidate
        autoComplete="off"
      >
        <span>
          請輸入手機號碼
        </span>  
        <TextField
          sx={{ml:20}}
          required
          onChange={(e) => setPhone(e.target.value)}
          className='Register-textField'
        />
        <Button variant="contained"  className='Success-btn-small' sx={{ml:2}}>發送驗證碼</Button> 
        <Box
          component="div"
          sx={{
            display: 'block'
          }}        
        >
          <Typography
            variant="h5"
            noWrap
            component="span"
            sx={{
              // mr: 2,
              // display: { xs: 'flex', md: 'none' },
             
              color: '#323232',
              fontSize: '1.5rem'
            }}
          >
            註冊即表示您已閱讀並同意 <Link onClick={()=>ChangRoute('/loginRegister')} sx={{color:'#0064C8',textDecoration: 'none'}}>《用戶註冊協議》</Link>
          </Typography>
        </Box>
      </Box>
      <Box
        className='Register-content'
        component="form"
        noValidate
        autoComplete="off"
      >
        <span>
          請輸入驗證碼
        </span>  
        <TextField
          sx={{ml:20}}
          required
          onChange={(e) => setCode(e.target.value)}
          className='Register-textField'
        />
        <Button variant="contained"  className='Success-btn-small' sx={{ml:2}}>重新發送</Button> 
      </Box>
      
      <Button  className='btn rounded btn-color-success' sx={{mt:4}} onClick={()=>ChangRoute('/Register')}><span className='btn-text' >同意</span></Button>
    </div>
    
  );
}



