import {useState}  from 'react';

import {Typography,FormControl,RadioGroup,FormControlLabel,Radio,Box,Divider,Select,MenuItem,Button } from '@mui/material';

import CreateIcon from '@mui/icons-material/Create';
import SearchIcon from '@mui/icons-material/Search';
import PhotoCamera from '@mui/icons-material/PhotoCamera';

const Info = () => {
  const [isEdit, setIsEdit] = useState(false);
  const [value, setValue] = useState('female');
 const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value);
  };
  
  return (
    <div >
      
      
      <Divider variant="middle"/>
      <Typography
        className='info-title'
        component="span"     
      >
        升級會員同意書
      </Typography>  
      <Box
        className='info-list'
        component="div"
      >
        <iframe src=''></iframe>
      </Box>
      <Button  className='btn rounded btn-color-cancel' sx={{mt:3, mb:5,mr:2}}><span className='btn-text'>不同意</span></Button>
      <Button  className='btn rounded btn-color-success' sx={{mt:3, mb:5}}><span className='btn-text'>同意</span></Button>
      
    </div>
  );
};
export default Info;


