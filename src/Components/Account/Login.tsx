import {useState, useEffect} from 'react';
import { useAppDispatch } from '../../app/hooks';
import {Typography,Box,TextField,Button,Divider,Link} from '@mui/material';
import {useNavigate} from 'react-router-dom'
import { RESTfulPost } from '../../features/API/RESTfulProcess';
import {
   SetToken
} from '../../features/Slice/TokenSlice';
const Login = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [Account, setAccount] = useState('');
  const [Password, setPassword] = useState('');
  function ChangRoute(path:string) {
    navigate(path)
  }
//  geosat
//  geosat@27285850
const login=async ()=>{
  const graphqlQuery = {
    query: `query login {
      login(model: { email: "${Account}", password: "${Password}" }) {
        userId
        token
      }
    }`,
    variables: {}
  };
  const response:any = await RESTfulPost('https://cloudtest.geosat.com.tw:8081/graphql',{Data:graphqlQuery});
  console.log(response)  
  if(!response.errors) {
    sessionStorage.setItem("token", encodeURIComponent(response.data.login.token));
    dispatch(SetToken(response.data.login.token))
    userCurrentRole(response.data.login.token)
    
    // createData('Cupcake', 305, 3.7, 67, 4.3,'審查',false)
  }
}
const userCurrentRole=async (token:string)=>{
  const graphqlQuery = {
    query: `query userCurrentRole {
      userCurrentRole {
        description
      }
    }`,
    variables: {}
  };
  const response:any = await RESTfulPost('https://cloudtest.geosat.com.tw:8081/graphql',{Token: token,Data:graphqlQuery});
  console.log(response)  
  if(!response.errors) {
    if(response.data.userCurrentRole.description==='管理者') {
      ChangRoute('/Backstage')
    } else {
      ChangRoute('/')
    }
    // createData('Cupcake', 305, 3.7, 67, 4.3,'審查',false)
  }
}
  return (
    <div className="Base">
      
      <Typography
        className='title'
        component="span"     
        sx={{
          fontWeight: 700,
        }}
      >
        登入
      </Typography>   
      <Box
        className='Login-content'
        component="form"
        noValidate
        autoComplete="off"
      >
        <span>
          請輸入手機號碼
        </span>  
        <TextField
          required
          type="tel"
          onChange={(e) => setAccount(e.target.value)}
          className='textField'
        />
      </Box>
      <Box
        className='Login-content'
        component="form"
        noValidate
        autoComplete="off"
      >
        <span>
          請輸入密碼
        </span>  
        <TextField
          sx={{mb:2}}
          required
          onChange={(e) => setPassword(e.target.value)}
          className='textField'
          type="password"
        />
        <Typography
          variant="h5"
          noWrap
          component="a"
          onClick={()=>ChangRoute('/Account/ResetPassword')}
          sx={{
            mr: 2,
            display: 'block',
            fontSize: '2rem',
            // display: { xs: 'flex', md: 'none' },
            fontWeight: 700,
            textDecoration: 'none',
          }}
        >
          忘記密碼
        </Typography>
      </Box>
       <Box
        className='Login-content'
        component="form"
        // sx={{
        //   '& .MuiTextField-root': { m: 1, width: '25ch' },
        // }}
        noValidate
        autoComplete="off"
      >          
        <Button  className='btn rounded btn-color-success' sx={{mt:6}} onClick={()=>login()}><span className='btn-text'>登入</span></Button>
        <Divider variant="middle"sx={{backgroundColor:'#1A8A80', maxWidth: '500px', mt:6,mb:6}}/>
        <div style={{display: 'inline'}}>
          <Typography
          variant="h5"
          noWrap
          component="span"
          sx={{
            fontSize: '2rem',
            // mr: 2,
            // display: { xs: 'flex', md: 'none' },
            fontWeight: 700,
            textDecoration: 'none',
          }}
        >
          還沒有帳號嗎?
        </Typography>
        <Link onClick={()=>ChangRoute('/VerifyMode')} sx={{color:'#EE9E1F',fontWeight: 700,textDecoration: 'none',ml:2,fontSize:'2rem'}}>註冊</Link>

        </div>
      </Box>
      
    </div>
  );
};
export default Login;
