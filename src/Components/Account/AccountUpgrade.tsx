import {useState, useEffect}  from 'react';

import {Typography,List,Box,IconButton,ListItem,ListItemText,TextField,FormControl,FormControlLabel,RadioGroup,Radio,Button } from '@mui/material';
import { useAppDispatch } from '../../app/hooks';
import CreateIcon from '@mui/icons-material/Create';
import SearchIcon from '@mui/icons-material/Search';
import ImgDialog from '../Dialog/ImgDialog'
import {
  SetImgDialog,
} from '../../features/Slice/ImgDialogSlice';
import {
    useNavigate,
} from 'react-router-dom'

import {AccountUpgradeDefinition} from '../../assets/config/interface';

const Info = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch();
  const [definitions, setDefinitions] = useState([
    {id:'1', name: '審核狀態',type: 'label'},
    {id:'2', name: '建立時間',type: 'label'},
    {id:'3', name: '審核時間',type: 'label'},
    {id:'4', name: '審核意見',type: 'label'},
    {id:'5', name: '升級身分',type: 'Radio', items:[{label:'合作會員',value: '合作會員'},{label:'廠商會員',value: '廠商會員'}]},
    {id:'6', name: '匯款帳號',type: 'text'},
    {id:'7', name: '存摺封面',type: 'image'},
    {id:'8', name: '證照號碼',type: 'text'},
    {id:'9', name: '證照圖片',type: 'image'},
    {id:'10', name: '營業登記',type: 'text'},
  ]);

  const [isEdit, setIsEdit] = useState(false);
  const [value, setValue] = useState({
    '1': '尚未送審',
    '2': '',
    '3': '',
    '4': '',
    '5': '廠商會員',
    '6': '',
    '7': '',
    '8': '',
    '9': '',
    '10': '',
  });
  useEffect(() => {
    switch(value['5']){
      case '合作會員':
        setDefinitions([
          {id:'1', name: '審核狀態',type: 'label'},
          {id:'2', name: '建立時間',type: 'label'},
          {id:'3', name: '審核時間',type: 'label'},
          {id:'4', name: '審核意見',type: 'label'},
          {id:'5', name: '升級身分',type: 'Radio', items:[{label:'合作會員',value: '合作會員'},{label:'廠商會員',value: '廠商會員'}]},
          {id:'6', name: '匯款帳號',type: 'text'},
          {id:'7', name: '存摺封面',type: 'image'},
          {id:'8', name: '證照號碼',type: 'text'},
          {id:'9', name: '證照圖片',type: 'image'},
       
        ])
        break
      case '廠商會員':
        setDefinitions([
          {id:'1', name: '審核狀態',type: 'label'},
          {id:'2', name: '建立時間',type: 'label'},
          {id:'3', name: '審核時間',type: 'label'},
          {id:'4', name: '審核意見',type: 'label'},
          {id:'5', name: '升級身分',type: 'Radio', items:[{label:'合作會員',value: '合作會員'},{label:'廠商會員',value: '廠商會員'}]},
          {id:'6', name: '匯款帳號',type: 'text'},
          {id:'7', name: '存摺封面',type: 'image'},
          {id:'10', name: '營業登記',type: 'text'},
        ])
        break
    }
    /* 上面是 componentDidMount和componentDidUpdate */
  }, [value]); /* 加入監控的props.dad */ 
  const handleChange = (status:string) => {
    navigate('/Account/Upgrade')
  };
  const GetButton = (status:string) => {
    let name = '';
    switch (status) {
      case '尚未送審':
        name='送審'
        break;
      case '通過審核':
      case '未通過審核':
        name='重新送審'
        break;
      case '尚未付款':
        name='付款'
        break;
      default:
        return ''
    }
    
    return <Button  className='btn rounded btn-color-success' sx={{ mb:5}} onClick={() =>handleChange(name)}><span className='btn-text'>{name}</span></Button>
  }
  function Greeting(props:AccountUpgradeDefinition) {
    const id = props.id;
    const text = props.text;
    const type = props.type;
    switch(type){
      case 'image':
        return  !isEdit
          ?<Button onClick={() => dispatch(SetImgDialog(true))} startIcon={<SearchIcon />} className='Success-btn-small' sx={{ml:5}} variant="contained">檢視</Button>
          :<input style={{marginLeft: '4rem'}} accept="image/*" type="file" />
      case 'label':
        return  <Typography
                sx={{ml:5}}
                // className='info-title'
                component="span"     
              >
                {text}
              </Typography>;
      case 'Radio':
        const items = props.items;
        return !(value['1']==='尚未送審'||value['1']==='不通過') 
        ?<Typography
            sx={{ml:5}}
            // className='info-title'
            component="span"     
            >
              {text}
            </Typography> 
        :<FormControl sx={{ml:5}}>
          <RadioGroup
            value={text}
            
            onChange={((event)=>{
            // value[id]=event.target.value;
            setValue(prevState  => { 
              let updatedValues=prevState ; 
              updatedValues[id as keyof typeof value]=event.target.value;  
              return {...prevState, ...updatedValues};}
            );
          })}  
          >
            {items.map((e)=>(<FormControlLabel key={e.label} value={e.value} control={<Radio />} label={e.label} />))}
          </RadioGroup>
        </FormControl>
      default:
        return !isEdit
          ?<Typography
            sx={{ml:5}}
            // className='info-title'
            component="span"     
            >
              {text}
            </Typography>
          :<TextField
            sx={{ml:5}}
            required
            // onChange={(e) => setCode(e.target.value)}
            className='textField'
            type="password"
            />; 
    }
  }
  return (
    <div >
      <Typography
        className='info-title'
        component="span"     
      >
        帳戶升級
      </Typography>   
      {(value['1']==='尚未付款'||value['1']==='審核中')?'':<IconButton className='Edit-btn'  onClick={(e)=>{setIsEdit(!isEdit)}}>
        <CreateIcon sx={{fontSize:'3rem'}}/>
      </IconButton>}
      <Box
        className='info-list'
        component="div"
      >
        <List
          component="div"
        >
          {definitions.map((e)=>(
            <ListItem key={e.name}>
              <ListItemText >{e.name}{<Greeting  text={value[e.id as keyof typeof value]} type={e.type} id={e.id} items={e.items||[]} />}  </ListItemText>   
            </ListItem>
          ))}
        </List>
      </Box>
      {GetButton(value['1'])}
      <ImgDialog path='https://source.unsplash.com/featured/?macbook'/>
    </div>
  );
};
export default Info;


