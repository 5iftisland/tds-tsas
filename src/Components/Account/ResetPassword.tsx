import {useState}  from 'react';

import {Typography,IconButton,RadioGroup,FormControlLabel,Radio,Box,TextField,Select,MenuItem,Button } from '@mui/material';

import CreateIcon from '@mui/icons-material/Create';
import SearchIcon from '@mui/icons-material/Search';
import PhotoCamera from '@mui/icons-material/PhotoCamera';

const Info = () => {
  const [isEdit, setIsEdit] = useState(false);
  const [value, setValue] = useState('female');
 const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value);
  };
  
  return (
    <div >
      
      <Typography
        className='info-title'
        component="span"     
      >
        重設密碼
      </Typography>   
      <Box
        className='ResetPassword-text'
        component="form"
        noValidate
        autoComplete="off"
        
      >
        <span>
          請輸入舊密碼
        </span>  
        <TextField
          
          required
          type="password"
          className='textField'
        />
      </Box> 
      <Box
        className='ResetPassword-text'
        component="form"
        noValidate
        autoComplete="off"
        
      >
        <span>
          請設定新密碼
        </span>  
        <TextField
          sx={{ml:21}}
          required
          type="password"
          className='textField'
        />
       <Box
        component="div"
        sx={{display: 'inline-block',fontSize:'2.5rem',textAlign:'left', ml:2 }}
       >
        <Typography
         
          component="span"     
          sx={{
            display: 'block',
            fontSize: '1.5rem',
            
          }}
        >
          ✓ 長度為6-12個字元
        </Typography> 
        <Typography
         
          component="span"     
          sx={{
            display: 'block',
            fontSize: '1.5rem',
          }}
        >
          ✓ 包含英文字元
        </Typography>
        <Typography
         
          component="span"     
          sx={{
            display: 'block',
            fontSize: '1.5rem',
          }}
        >
          ✓ 包含數字
        </Typography>
       </Box>
      </Box>
      <Box
        className='ResetPassword-text'
        component="form"
        noValidate
        autoComplete="off"
        
      >
        <span>
          請確認新密碼
        </span>  
        <TextField
          
          required
          type="password"
          className='textField'
        />
      </Box>

      <Button  className='btn rounded btn-color-cancel' sx={{mt:3, mb:5,mr:2}}><span className='btn-text'>取消</span></Button>
      <Button  className='btn rounded btn-color-success' sx={{mt:3, mb:5}}><span className='btn-text'>確認</span></Button>
    </div>
  );
};
export default Info;

