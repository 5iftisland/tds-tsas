import {useState}  from 'react';

import {Typography,List,Box,IconButton,ListItem,ListItemText,TextField,Select,MenuItem,Button } from '@mui/material';

import CreateIcon from '@mui/icons-material/Create';
import SearchIcon from '@mui/icons-material/Search';
import PhotoCamera from '@mui/icons-material/PhotoCamera';

import {UserInfoItem} from '../../assets/config/interface';
const Info = () => {
  //  const handleChange = (event: SelectChangeEvent) => {
  //   setAge(event.target.value as string);
  // };
  const definitions=[
    {id:'1', name: '會員身份',type: 'label'},
    {id:'2', name: '真實姓名',type: 'text'},
    {id:'3', name: '手機號碼',type: 'label'},
    {id:'4', name: '電子郵件',type: 'text'},
    {id:'5', name: '性別　　',type: 'select',items:[{label:'男',value: '男'},{label:'女',value: '女'}]},
    {id:'6', name: '生日　　',type: 'text'},
    {id:'7', name: '城市　　',type: 'text'},
    {id:'8', name: '行政區　',type: 'text'},
    {id:'9', name: '地址　　',type: 'text'},
    {id:'10', name: '組織　　',type: 'text'},
    // {id:'11', name: '帳戶升級',type: 'button'},
    // {id:'12', name: '匯款帳號',type: 'text'},
    // {id:'13', name: '存摺封面',type: 'image'},
    // {id:'14', name: '證照號碼',type: 'text'},
    // {id:'15', name: '證照圖片',type: 'image'},
    // {id:'16', name: '營業登記',type: 'text'},
    // {id:'11', name: '證明類型',type: 'select',items:[{label:'身分證字號',value: '身分證字號'},{label:'護照號碼',value: '護照號碼'}]},
    // {id:'12', name: '身份證明',type: 'Text'},  
  ]
  const [isEdit, setIsEdit] = useState(false);
  const [value, setValue] = useState({
    '1': '123',
    '2': '',
    '3': '',
    '4': '',
    '5': '男',
    '6': '',
    '7': '',
    '8': '',
    '9': '',
    '10': '',
    '11': '',
    '12': '',
    '13': '',
    '14': '',
    '15': '',
    '16': ''
  });
 
  // const handleChange = (event: SelectChangeEvent) => {
  //   setAge(event.target.value as string);
  // };
  function Greeting(props:UserInfoItem) {
    const id = props.id;
    const text = props.text;
    const type = props.type;
  
    switch(type){
       case 'select':
        const items = props.items;
        return <Select
          sx={{ml:5,fontSize:'1.8rem'}}
          value={text}           
          onChange={((event)=>{
            // value[id]=event.target.value;
            setValue(prevState  => { 
              let updatedValues=prevState ; 
              updatedValues[id as keyof typeof value]=event.target.value;  
              return {...prevState, ...updatedValues};}
            );
          })}    
        >
          {items.map((e)=>(<MenuItem key={e.label} sx={{fontSize:'1.5rem'}} value={e.value}>{e.label}</MenuItem>))}
        </Select>
      case 'image':
        return  !isEdit
                ?<Button startIcon={<SearchIcon />} className='Success-btn-small' sx={{ml:5}} variant="contained">檢視</Button>
                :<IconButton color="primary" aria-label="upload picture" component="label">
                  <input hidden accept="image/*" type="file" />
                  <PhotoCamera />
                </IconButton>      
      case 'label':
        return  <Typography
                sx={{ml:5}}
                // className='info-title'
                component="span"     
              >
                {text}
              </Typography>;
      default:
        return!isEdit
          ?<Typography
              sx={{ml:5}}
              // className='info-title'
              component="span"     
            >
              {text}
            </Typography>
          : <TextField
              sx={{ml:5}}
              required
              // onChange={(e) => setCode(e.target.value)}
              className='textField'
              type="password"
            />
    }

  }
  return (
    <div >
      
      <Typography
        className='info-title'
        component="span"     
      >
        資訊
      </Typography>   
      <IconButton className='Edit-btn'  onClick={(e)=>{setIsEdit(!isEdit)}}>
        <CreateIcon sx={{fontSize:'3rem'}}/>
      </IconButton>
      <Box
        className='info-list'
        component="div"
      >
        <List
          
          component="div"
        >
          {definitions.map((e)=>(
            <ListItem key={e.name}>
              <ListItemText >{e.name}{<Greeting  text={value[e.id as keyof typeof value]} type={e.type} items={e.items||[]} id={e.id} />}  </ListItemText>   
            </ListItem>
          ))}
        </List>
      </Box>
      
      {isEdit?<Button  className='btn rounded btn-color-success' sx={{mt:3,mb:5}}><span className='btn-text'>確認</span></Button>: ''}
    </div>
  );
};
export default Info;

