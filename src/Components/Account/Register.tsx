import {useState} from 'react';
import {Typography,Box,TextField,Button} from '@mui/material';

export default function CountrySelect() {
  // const [Phone, setPhone] = useState('');
  const [Code, setCode] = useState('');
  // function inputProcess(e:any) {
  //   console.log(e.target.value)
  // }
  // const style = {
  //   fontSize:'1rem'
  // };
  return (
    <div className="Base">
      <Typography
        className='title'
        component="span"     
        sx={{
          fontWeight: 700,
        }}
      >
        註冊
      </Typography> 
      
      <Box
        className='Register-content'
        component="form"
        noValidate
        autoComplete="off"
        
      >
        <span>
          請設定密碼
        </span>  
        <TextField
          sx={{ml:21}}
          required
          type="password"
          onChange={(e) => setCode(e.target.value)}
          className='textField'
        />
       <Box
        component="div"
        sx={{display: 'inline-block',fontSize:'2.5rem',textAlign:'left', ml:2 }}
       >
        <Typography
         
          component="span"     
          sx={{
            display: 'block',
            fontSize: '1.5rem',
            
          }}
        >
          ✓ 長度為6-12個字元
        </Typography> 
        <Typography
         
          component="span"     
          sx={{
            display: 'block',
            fontSize: '1.5rem',
          }}
        >
          ✓ 包含英文字元
        </Typography>
        <Typography
         
          component="span"     
          sx={{
            display: 'block',
            fontSize: '1.5rem',
          }}
        >
          ✓ 包含數字
        </Typography>
       </Box>
       
      </Box>
      <Box
        className='Register-content'
        component="form"
        noValidate
        autoComplete="off"
      >
        <span>
          請確認密碼
        </span>  
        <TextField
          sx={{ml:3}}
          required
          type="password"
          onChange={(e) => setCode(e.target.value)}
          className='textField'
        />
       
       
      </Box>
      <Box
        className='Register-content'
        component="form"
        noValidate
        autoComplete="off"
      >
        <span>
          請設定電子郵件
        </span>  
        <TextField
          sx={{ml:3}}
          required
          type="email"
          onChange={(e) => setCode(e.target.value)}
          className='textField'
        />
      </Box>
      <Box
        sx={{mt:4}}
        component="form"
        noValidate
        autoComplete="off"
      >
        <Button  className='btn rounded btn-color-success' sx={{mt:6}}><span className='btn-text'>確認</span></Button>
        <Button  className='btn rounded btn-color-cancel' sx={{mt:6}}><span className='btn-text'>清除</span></Button>
       
      </Box>
    </div>
    
  );
}



