import * as React from 'react';
import {
    Outlet,
    useLocation,
    useNavigate
} from 'react-router-dom'
import {Typography,Box,Avatar,IconButton,List,ListItemButton,ListItemIcon,ListItemText} from '@mui/material';
import PersonIcon from '@mui/icons-material/Person';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import SendIcon from '@mui/icons-material/Send';
import DraftsIcon from '@mui/icons-material/Drafts';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
const Info = () => {
  const location = useLocation()
  const navigate = useNavigate();
  function ChangRoute(path:string) {
    navigate(path)
  }
  const isFocus=(pathKey:string):string=>{
    return location.pathname.indexOf(pathKey)>-1? 'tabs-list-item-focus': ''
  }
  

  return (
    <div className="Base">
      <Typography
        className='title'
        component="span"     
        sx={{
          
          fontWeight: 700,
        }}
      >
        田間管理
      </Typography>   
      <Box
        className='Tabs'
        component="div"
      >
        <Box
          className='tabs-left-list'
          component="div"
        >
          <List
            
            component="div"
          >
            <ListItemButton onClick={()=>ChangRoute('/Farm/Crop')} className={'tabs-list-item '+isFocus('Crop')}>
              <ListItemIcon>
                <SendIcon />
              </ListItemIcon>
              <ListItemText primary="作物" />
              <ArrowForwardIosIcon />
            </ListItemButton>
            <ListItemButton onClick={()=>ChangRoute('/Farm/Farmland')} className={'tabs-list-item '+isFocus('Farmland')}>
              <ListItemIcon>
                <DraftsIcon />
              </ListItemIcon>
              <ListItemText primary="農地" />
              <ArrowForwardIosIcon />
            </ListItemButton>
            <ListItemButton onClick={()=>ChangRoute('/Farm/Materials')} className={'tabs-list-item '+isFocus('Materials')}>
              <ListItemIcon>
                <DraftsIcon />
              </ListItemIcon>
              <ListItemText primary="資材" />
              <ArrowForwardIosIcon />      
            </ListItemButton>
            <ListItemButton onClick={()=>ChangRoute('/Farm/Materials')} className={'tabs-list-item '+isFocus('Materials')}>
              <ListItemIcon>
                <DraftsIcon />
              </ListItemIcon>
              <ListItemText primary="農事" />
              <ArrowForwardIosIcon />      
            </ListItemButton>
          </List>
        </Box>
        <Box
          className="info-content"
          component="div"
        >
          <Outlet/>
        </Box>
        
      </Box>   
    </div>
  );
};
export default Info;
