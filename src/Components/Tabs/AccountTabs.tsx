import * as React from 'react';
import {
    Outlet,
    useLocation,
    useNavigate
} from 'react-router-dom'
import {Typography,Box,Avatar,IconButton,List,ListItemButton,ListItemIcon,ListItemText} from '@mui/material';
import PersonIcon from '@mui/icons-material/Person';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import SendIcon from '@mui/icons-material/Send';
import DraftsIcon from '@mui/icons-material/Drafts';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

const AccountTabs = () => {
  const location = useLocation()
  const isFocus=(pathKey:string):string=>{
    return location.pathname.indexOf(pathKey)>-1? 'tabs-list-item-focus': ''
  }
  const navigate = useNavigate();
  function ChangRoute(path:string) {
    navigate(path)
  }
  return (
    <div className="Base">
      <Typography
        className='title'
        component="span"     
        sx={{
          fontWeight: 700,
        }}
      >
        帳戶
      </Typography>   
      <Box
        className='Tabs'
        component="div"
      >
        <Box
          className='tabs-left-list'
          component="div"
        >
          <Avatar className='avatar'>
            <PersonIcon ></PersonIcon>
          </Avatar>
          {/* <IconButton color="primary" aria-label="upload picture" component="label" className='avatar-btn'>
            <input hidden accept="image/*" type="file" />
            <PhotoCamera sx={{fontSize:'3rem'}}/>
          </IconButton> */}
          <Typography
            className='user-name'
            component="div"     
            sx={{
              fontWeight: 700,
            }}
          >
            王小霏
          </Typography> 
          <List
            component="div"
          >
            <ListItemButton onClick={()=>ChangRoute('/Account/Information')} className={'tabs-list-item '+isFocus('Information')}>
              <ListItemIcon>
                <SendIcon />
              </ListItemIcon>
              <ListItemText primary="資訊" />
              <ArrowForwardIosIcon />
            </ListItemButton>
            <ListItemButton onClick={()=>ChangRoute('/Account/ResetPassword')} className={'tabs-list-item '+isFocus('ResetPassword')}>
              <ListItemIcon>
                <DraftsIcon />
              </ListItemIcon>
              <ListItemText primary="密碼" />
              <ArrowForwardIosIcon />
            </ListItemButton>
            <ListItemButton onClick={()=>ChangRoute('/Account/Review')} className={'tabs-list-item '+isFocus('Review')}>
              <ListItemIcon>
                <DraftsIcon />
              </ListItemIcon>
              <ListItemText primary="升級"  />
              <ArrowForwardIosIcon />      
            </ListItemButton>
          </List>
        </Box>
        <Box
          className="info-content"
          component="div"
        >
          <Outlet/>
        </Box>
        
      </Box>   
    </div>
  );
};
export default AccountTabs;
