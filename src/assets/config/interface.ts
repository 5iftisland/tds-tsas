export interface UserInfoItem {
  id: string,
  text: string,
  type: string,
  items:{
      label: string;
      value: string;
  }[] | [],
  cellbake?: Function
}

export interface CountryType {
  code: string;
  label: string;
  phone: string;
  suggested?: boolean;
}

export interface AccountUpgradeDefinition {
  id: string,
  text: string,
  type: string,
  items:{
      label: string;
      value: string;
  }[] | [],
}

export interface AccountTableRow {
  calories: number;
  carbs: number;
  fat: number;
  name: string; //必要欄位
  protein: number;
  select: string;
  edit: string;
  checkbox: boolean;
}

export interface AccountReviewTableRow {
  id: string
  displayName: string|null;
  Name: string;
  email: string|null;
  phoneNumber: string|null; 
  description: string|null; 
  // select: string;
  edit: string;
  checkbox: boolean;
}

export interface CropDefinition {
  id: string,
  text: string,
  type: string,
  items:{
      label: string;
      value: string;
  }[] | [],
  cellbake?: Function

}

export interface FarmlandDefinition {
  id: string,
  text: string,
  type: string,
  items:{
      label: string;
      value: string;
  }[] | [],
  cellbake?: Function

}

export interface MaterialsDefinition {
  id: string,
  text: string,
  type: string,
  items:{
      label: string;
      value: string;
  }[] | [],
  cellbake?: Function

}

export interface userRoleAudit {
  type: string,
  comment: string | null,
  isPaid: boolean,
  createdOn: string | null,
  reviewOn: string | null,
  user: {
    id: string,
    displayName: string | null,
    lastName: string | null,
    firstName: string | null,
    email: string | null,
    phoneNumber: string | null,
    userProfileExtendedAttributeResponse: {
      bandCode: string | null,
      bankAccount: string | null,
      bankbookUrl: string | '',
      licenseCode: string | null,
      licenseUrl: string | ''
    }
  },
  role: {
    id:string | null,
    description: string | null
  }
}
    
