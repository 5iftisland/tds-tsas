# [1.1.0](https://gitlab.com/5iftisland/tds-tsas/compare/v1.0.0...v1.1.0) (2022-08-24)


### ✨ Features

* CI/CD (by @PTwo) ([c50e408](https://gitlab.com/5iftisland/tds-tsas/commit/c50e408de94013ff58dba1048dda1efd4f8e6afc))

# 1.0.0 (2022-08-24)


### ✨ Features

* 初始Rudex、Route及webpage設定 (by @PTwo) ([20f52e3](https://gitlab.com/5iftisland/tds-tsas/commit/20f52e30431e05e036e91a0c5112fd0a0f7d099d))
